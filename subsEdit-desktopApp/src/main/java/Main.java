import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;

public class Main extends Application {
    final static Logger logger = Logger.getLogger(Main.class);


    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Subtitle TransformationProcess");

        try {

            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/fxml/SubtitleTransformerView.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Scene scene = new Scene(page);
            primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/images/chicken.png")));
            primaryStage.setTitle("SubsEdit");
            primaryStage.setResizable(false);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

}


