package subsedit.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import subsedit.history.HistoryHelper;
import subsedit.history.TransformationResultHistory;
import subsedit.transformation.DefaultTransformation;
import subsedit.transformation.MultiTransformation;
import subsedit.transformation.TransformationConfig;
import subsedit.alerts.ExceptionAlert;
import subsedit.alerts.InformationAlert;

import java.io.File;
import java.util.Date;

public class SubtitleEditorController {
    final static Logger logger = Logger.getLogger(SubtitleEditorController.class);
    @FXML
    private Button selectFileButton;

    @FXML
    private Slider sliderSubsToRemove;

    @FXML
    private Button buttonRemoveSubs;

    @FXML
    private Label labelEffectivePercent;
    @FXML
    private PieChart pieChartSubsToRemove;

    @FXML
    private RadioButton folderRadio;
    @FXML
    private RadioButton fileRadio;

    @FXML
    private TableView<TransformationResultHistory> historyTable;

    @FXML
    private TableColumn<TransformationResultHistory, String> nameColumn;

    @FXML
    private TableColumn<TransformationResultHistory, Date> dateColumn;

    @FXML
    private TableColumn<TransformationResultHistory, Number> percentageColumn;

    ExceptionAlert alertError = ExceptionAlert.getINSTANCE();
    InformationAlert alertInfo = InformationAlert.getINSTANCE();
    private boolean isFileToConvert;
    private int percentageToRemove;
    private boolean isChooserShown = false;
    private FileChooser fileChooser = new FileChooser();
    private DirectoryChooser directoryChooser = new DirectoryChooser();
    final ToggleGroup radioFileGroup = new ToggleGroup();
    private File file;

    @FXML
    private void initialize() throws Exception {
        initializeSlider();
        initializePieChart();
        initializeFileChooser();
        initializeTableView();
        initializeRadioButtons();
        buttonRemoveSubs.setDisable(true);
    }


    @FXML
    private void selectFile() {
        if (!isChooserShown) {

            isChooserShown = true;
            File selectedFile;
            if (isFileToConvert) {
                selectedFile = fileChooser.showOpenDialog(new Stage());
            } else {
                selectedFile = directoryChooser.showDialog(new Stage());
            }
            isChooserShown = false;
            if (selectedFile != null) {
                buttonRemoveSubs.setDisable(false);
                file = selectedFile;
            }
        } else {
            alertInfo.showDialog("FileChooser already in use", "Sorry, it's not possible to use multiple FileChoosers at the same time .");
        }

    }


    @FXML
    private void removeSubs() {
        try {

            MultiTransformation transformation = new MultiTransformation();
            if (isFileToConvert) {
                transformation.addTransformation(new TransformationConfig(file, percentageToRemove, true));
                //    multiTransformation.addFile(file);
            } else {
                transformation.addDirectory(file, percentageToRemove, true);
            }
            transformation.perform();
            alertInfo.showDialog("Subtitles successfully modified !", "The subtitles file(s) has/have been replaced by the new one(s), but you  can still find the old one(s) in the 'oldSubs' directory ");
            //Map<File, TransformationResult> result = multiTransformation.getResults();
            for (DefaultTransformation t : transformation.getTransformations()) {
                HistoryHelper.save(new TransformationResultHistory(t.getResult()));
            }
        } catch (Exception ex) {
            logger.error(ex);
            alertError.showDialog(ex);
            ex.printStackTrace();
        }
        buttonRemoveSubs.setDisable(true);

    }

    /**
     * initializes the tableView (history)
     */

    private void initializeTableView() {
        historyTable.setItems(HistoryHelper.getHistory());
        nameColumn.setCellValueFactory(new PropertyValueFactory<TransformationResultHistory, String>("fileName"));
        percentageColumn.setCellValueFactory(new PropertyValueFactory<TransformationResultHistory, Number>("percentOfSuppression"));
        dateColumn.setCellValueFactory(new PropertyValueFactory<TransformationResultHistory, Date>("transformationDate"));

    }

    /**
     * initializes the PieChart
     */
    private void initializePieChart() {
        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                        new PieChart.Data("To Keep", 100),
                        new PieChart.Data("To Remove", 0)
                );
        pieChartSubsToRemove.setData(pieChartData);
    }

    /**
     * Initializes the FileChooser
     */
    private void initializeFileChooser() {
        //init the single file chooser


        fileChooser.getExtensionFilters().setAll(
                new FileChooser.ExtensionFilter("SRT", "*.srt")
        );


    }

    /**
     * Initializes the Slider
     */
    private void initializeSlider() {
        sliderSubsToRemove.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable,
                                Number oldValue, Number newValue) {
                percentageToRemove = (int) sliderSubsToRemove.getValue();
                labelEffectivePercent.setText(Integer.toString(percentageToRemove) + " %");
                pieChartSubsToRemove.getData().get(0).setPieValue(100 - percentageToRemove);
                pieChartSubsToRemove.getData().get(1).setPieValue(percentageToRemove);

            }
        });
    }

    /**
     * Initializes the RadioButton, permits to know if a file or a directory needs to be transformed
     */

    private void initializeRadioButtons() {
        folderRadio.setToggleGroup(this.radioFileGroup);
        fileRadio.setToggleGroup(this.radioFileGroup);
        this.radioFileGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (fileRadio.isSelected()) {
                    selectFileButton.setText("Select your subtitle file !");
                    isFileToConvert = true;
                } else {
                    selectFileButton.setText("Select your subtitles folder !");
                    isFileToConvert = false;
                }
                buttonRemoveSubs.setDisable(true);
            }
        });

        fileRadio.setSelected(true);

    }

}

