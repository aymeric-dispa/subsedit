package subsedit.alerts;

import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * InformationAlert permits to manage all the Information to show.
 */
public class InformationAlert extends Alert {
    private static InformationAlert INSTANCE = new InformationAlert();
    private final String ICON_PATH="/images/chicken.png";

    /**
     * Constructor
     */
    private InformationAlert() {
        super(AlertType.INFORMATION);
        Stage stage = (Stage) this.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream(ICON_PATH)));
        this.setHeaderText(null);
    }
    /**
     * Show the Dialog
     */
    public void showDialog(String alertTitle, String alertInfo) {
        this.setTitle(alertTitle);
        this.setContentText(alertInfo);
        this.showAndWait();
    }

    /**
     *
     * @return the instance (Singleton)
     */
    public static InformationAlert getINSTANCE() {
        return INSTANCE;
    }
}
