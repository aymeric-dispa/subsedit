package subsedit.alerts;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * ExceptionAlert permits to manage all the Errors to show.
 */
public class ExceptionAlert extends Alert {
    private static ExceptionAlert INSTANCE = new ExceptionAlert();
    private TextArea textArea = new TextArea();
    private final String TITLE="Oops, a wild exception appeared !";
    private final String ICON_PATH="/images/chicken.png";


    /**
     * Constructor
     */
    private ExceptionAlert() {
        super(AlertType.ERROR);
        Stage stage = (Stage) this.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream(ICON_PATH)));
        this.setTitle(TITLE);
        this.setHeaderText(null);
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);
        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        Label label = new Label("The exception stacktrace was:");
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);
        this.getDialogPane().setExpandableContent(expContent);
    }

    /**
     * Show the Dialog
     * @param exception
     */
    public void showDialog(Exception exception) {
        this.setContentText(exception.getMessage());
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        String exceptionText = sw.toString();
        textArea.setText(exceptionText);
        this.showAndWait();
    }

    /**
     * @return the instance (Singleton)
     */
    public static ExceptionAlert getINSTANCE() {
        return INSTANCE;
    }


}
