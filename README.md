# SubsEdit
A subtitle editor that permits to remove a part of the subtitles (useful for learning a new language by watching a movie/tv show).

![screenshot](https://cloud.githubusercontent.com/assets/10419887/11825688/df54dfb8-a380-11e5-902f-3ea88f438bf3.png)
