package subsedit.util;

/**
 * Created by Aymeric on 29-07-16.
 */
public class Constants {
    public final static String WORKING_DIRECTORY_NAME ="SubsEdit";

    //length in bytes.
    public final static int MAXLENGTH_SUBS_DIRECTORY_BYTES =250000000;
}
