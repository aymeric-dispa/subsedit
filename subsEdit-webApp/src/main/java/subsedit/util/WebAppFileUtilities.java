package subsedit.util;

import org.apache.commons.io.FilenameUtils;
import subsedit.transformation.DefaultTransformation;
import subsedit.transformation.TransformationResult;
import subsedit.utils.InvalidSubsFileException;
import java.io.*;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import static subsedit.util.Constants.*;

/**
 * Utilities class for file and directory processes
 */
public class WebAppFileUtilities {

    /**
     * Create a working directory if it does not exist, and check its length if it does exist.
     */
    private static File workDir;

    static {
        workDir = new File(WORKING_DIRECTORY_NAME);
        if (!workDir.exists()) {
            workDir.mkdir();
        } else {
            checkSubsDirectoryLength();
        }
    }


    /**
     * Check the length of the directory which contains the subs.
     * Will remove half of the content if the size is bigger than the limit.
     */
    public static synchronized void checkSubsDirectoryLength() {
        //daysBack is the number of days of history we will try to keep.
        int daysBack = 7;

        //When the length > maxLength, we remove half of it.
        while (workDir.length() > MAXLENGTH_SUBS_DIRECTORY_BYTES / 2 && daysBack >= 0) {
            deleteOldSubs(daysBack, MAXLENGTH_SUBS_DIRECTORY_BYTES / 2, workDir);
            daysBack--;
        }
    }


    /**
     * Remove the olds subs zip files.
     *
     * @param maxLength
     * @param directory
     */
    private static void deleteOldSubs(int daysBack, int maxLength, File directory) {
        if (directory.exists()) {
            File[] listFiles = directory.listFiles();
            long purgeTime = System.currentTimeMillis() - (daysBack * 24 * 60 * 60 * 1000);
            for (File listFile : listFiles) {
                if (listFile.lastModified() < purgeTime) {
                    if (!listFile.delete()) {
                        System.err.println("Unable to delete file: " + listFile);
                    }
                }
                if (directory.length() < maxLength) {
                    return;
                }
            }
        }
    }

    /**
     * Generate a ZipFile which will contain the transformed zip files.
     *
     * @param transformations
     * @param destinationWay
     * @return
     */
    public static File generateZip(List<DefaultTransformation> transformations, String destinationWay) {
        File destination = new File(destinationWay);
        transformations.get(0).getConfig().getFile();
        byte[] buffer = new byte[1024];
        try {
            String zipName = new BigInteger(130, new SecureRandom()).toString(32) + ".zip";
            Path path = Paths.get(destination.getAbsolutePath(), zipName);
            FileOutputStream fos = new FileOutputStream(path.toFile());
            ZipOutputStream zos = new ZipOutputStream(fos);
            for (DefaultTransformation transformation : transformations) {
                TransformationResult result = transformation.getResult();
                ZipEntry ze = new ZipEntry(transformation.getConfig().getName());
                zos.putNextEntry(ze);
                InputStream is = new ByteArrayInputStream(result.getSubStream());
                int len;
                while ((len = is.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                is.close();
            }
            zos.closeEntry();
            zos.close();
            return path.toFile();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Validate the subtitles File.
     *
     * @param sub
     * @param s
     * @param l
     * @throws InvalidSubsFileException
     */
    public static void validateFile(File sub, String s, long l) throws InvalidSubsFileException {
        if (!FilenameUtils.getExtension(s).equals("srt")) {
            throw new InvalidSubsFileException("The extension of the sub file must be .srt");
        }

        if (l <= 0 || sub.length() <= 0) {
            throw new InvalidSubsFileException("The file cannot be empty");
        }
    }

}
