package subsedit.util;


/**
 * Basic subtitle information - used to identify a subs file to transform
 */
public class Subtitle {
    private String name;

    public Subtitle(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) return true;
        if (!(obj instanceof Subtitle)) return false;
        if (this.name.equals(((Subtitle) obj).getName())) {
            return true;
        }
        return false;
    }




}
