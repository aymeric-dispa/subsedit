package subsedit.webapp;

import subsedit.utils.TransformationException;
import subsedit.util.Subtitle;
import subsedit.utils.InvalidSubsFileException;

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * SubsEdit View Interface - MVP design pattern
 */
public interface SubsEditView {
    void downloadButtonClick(File resource);
    void updateGrid(Set<Subtitle> subs);
    void addListener(SubsEditViewListener listener);
    void changePercentage(int percent) throws IOException, TransformationException;
    interface SubsEditViewListener {
        void downloadButtonClick() throws IOException, TransformationException;
        void changePercentage(int percentage) ;
        void addSub(String name, File file, long size) throws InvalidSubsFileException, IOException;
        void removeSub(Subtitle subtitle);
    }

}
