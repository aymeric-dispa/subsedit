package subsedit.webapp;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import javax.servlet.annotation.WebServlet;


@Theme("mytheme")
@Widgetset("subsedit.MyAppWidgetset")
/**
 * SubsEdit UI
 */
public class SubsEditUI extends UI {

    protected Button downloadButton = new Button("Download Subtitle(s)");

    public void init(VaadinRequest vaadinRequest) {

        // Create the content root layout for the UI
        VerticalLayout content = new VerticalLayout();
        setContent(content);

        // Create the model and the Vaadin view implementation
        SubsEditViewImpl view = new SubsEditViewImpl();
        SubsEditModel model = new SubsEditModel();

        // The presenter binds the model and view together
        new SubsEditPresenter(model, view);


        content.addComponent(view);
    }


    @WebServlet(urlPatterns = "/*", asyncSupported = true)
    @VaadinServletConfiguration(ui = SubsEditUI.class, productionMode = true)
    public static class MyUIServlet extends VaadinServlet {
    }

}
