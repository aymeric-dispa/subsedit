package subsedit.webapp;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.*;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.ClickableRenderer;
import org.vaadin.easyuploads.MultiFileUpload;
import subsedit.utils.TransformationException;
import subsedit.util.Subtitle;
import subsedit.utils.InvalidSubsFileException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * SubsEditView Implementation - MVP design pattern
 */
public class SubsEditViewImpl extends CustomComponent implements SubsEditView {
    private List<SubsEditView.SubsEditViewListener> listeners = new ArrayList<SubsEditView.SubsEditViewListener>();
    private Grid grid = new Grid();
    private final Label percentLabel = new Label("");
    private final Label percentSliderTitle = new Label("Percentage to remove : ");
    private int percentage = 20;
    private File zipFile;
    private FileDownloader fileDownloader;
    private Button downloadButton = new Button("Download Subtitle(s)");

    public SubsEditViewImpl() {
        ButtonRenderer deleteButton = createDeleteButton();
        Slider percentSlider = createPercentSlider();
        MultiFileUpload uploader = createUploader();

        //need to update the grid a first time for initialization.
        updateGrid(null);
        grid.getColumn("delete").setRenderer(deleteButton);
        fileDownloader = new FileDownloader(generateResource());
        fileDownloader.extend(downloadButton);


        //create the main layout
        VerticalLayout content = new VerticalLayout();

        //layout which contains the uploader and the percentage - in other words : all the configuration information
        HorizontalLayout configHorizontalLayout = new HorizontalLayout();
        configHorizontalLayout.addComponent(uploader);

        //Configure the layout which contains the percentage slider
        AbsoluteLayout sliderLayout = new AbsoluteLayout();
        sliderLayout.setWidth("100px");
        sliderLayout.setHeight("35px");
        sliderLayout.addComponent(percentSlider);

        //Configure the layout which contains the percentage slider layout and the labels linked to the percentage
        HorizontalLayout percentageLayout = new HorizontalLayout();
        percentageLayout.addComponent(percentSliderTitle);
        percentageLayout.addComponent(sliderLayout);
        percentageLayout.addComponent(percentLabel);
        percentageLayout.setComponentAlignment(percentSliderTitle, Alignment.MIDDLE_LEFT);
        percentageLayout.setComponentAlignment(percentLabel, Alignment.MIDDLE_LEFT);

        //add the percentage Layout to the layout which contains the uploader
        configHorizontalLayout.addComponent(percentageLayout);
        configHorizontalLayout.setComponentAlignment(percentageLayout, Alignment.BOTTOM_LEFT);

        //add every layouts to the principal one
        content.addComponent(configHorizontalLayout);
        content.addComponent(new Label("&nbsp;", Label.CONTENT_XHTML));
        content.addComponents(grid);
        content.addComponent(downloadButton);
        setCompositionRoot(content);
    }


    private Slider createPercentSlider() {
        final Slider percentSlider = new Slider(1, 100);
        percentSlider.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                percentLabel.setValue(Double.toString(percentSlider.getValue()) + " %");
                changePercentage((int) ((double) percentSlider.getValue()));
            }
        });
        percentSlider.setValue(50d);
        return percentSlider;
    }

    private MultiFileUpload createUploader() {
        return new MultiFileUpload() {
            @Override
            protected void handleFile(File sub, String s, String s1, long l) {
                try {
                    for (SubsEditView.SubsEditViewListener listener : listeners)
                        listener.addSub(s, sub, l);
                } catch (InvalidSubsFileException | IOException ex) {
                    Notification.show(ex.getMessage(), Notification.Type.WARNING_MESSAGE);
                    ex.printStackTrace();
                }
            }

        };
    }


    private ButtonRenderer createDeleteButton() {
        ButtonRenderer buttonRenderer = new ButtonRenderer();
        buttonRenderer.addClickListener(new ClickableRenderer.RendererClickListener() {
            @Override
            public void click(ClickableRenderer.RendererClickEvent rendererClickEvent) {
                for (SubsEditView.SubsEditViewListener listener : listeners)
                    listener.removeSub((Subtitle) rendererClickEvent.getItemId());
            }
        });
        return buttonRenderer;
    }

    private StreamResource generateResource() {
        /**
         * This method is called before any listener on the download button, that's why
         * we have to refresh the view there, in order to get the last version of the zipFile.
         */
        return new StreamResource(new StreamResource.StreamSource() {
            @Override
            public InputStream getStream() {
                try {
                    refreshViewForDownload();
                    zipFile.deleteOnExit();
                    return new FileInputStream(zipFile);
                } catch (IOException | TransformationException ex) {
                    Notification.show(ex.getMessage(), Notification.Type.WARNING_MESSAGE);
                    ex.printStackTrace();
                }
                return null;
            }
        }, "subsEdit" + new SimpleDateFormat("dd-MM_HH-mm").format(new Date()) + ".zip");
    }

    /*
    * Refresh the view to get a new version of the zipFile.
     */
    private void refreshViewForDownload() throws IOException, TransformationException {
        for (SubsEditView.SubsEditViewListener listener : listeners)
            listener.downloadButtonClick();
    }


    @Override
    public void downloadButtonClick(File zipFile) {
        this.zipFile = zipFile;
    }

    @Override
    public void updateGrid(Set<Subtitle> subs) {
        BeanItemContainer<Subtitle> subsContainer = new BeanItemContainer<>(Subtitle.class, subs);
        GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(subsContainer);
        gpc.addGeneratedProperty("delete",
                new PropertyValueGenerator<String>() {
                    @Override
                    public String getValue(Item item, Object itemId,
                                           Object propertyId) {
                        return "Delete";
                    }

                    @Override
                    public Class<String> getType() {
                        return String.class;
                    }
                });
        grid.setContainerDataSource(gpc);
        downloadButton.setEnabled(subs != null && subs.size() > 0);
    }

    public void addListener(SubsEditView.SubsEditViewListener listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }

    @Override
    public void changePercentage(int percent) {
        for (SubsEditView.SubsEditViewListener listener : listeners)
            listener.changePercentage(percent);
    }


}
