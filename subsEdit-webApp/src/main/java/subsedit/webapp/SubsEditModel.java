package subsedit.webapp;

import subsedit.utils.TransformationException;
import subsedit.transformation.MultiTransformation;
import subsedit.transformation.TransformationConfig;
import subsedit.util.Subtitle;
import subsedit.util.WebAppFileUtilities;
import subsedit.utils.InvalidSubsFileException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static subsedit.util.Constants.WORKING_DIRECTORY_NAME;

/**
 * SubsEdit Model - MVP design pattern
 */
public class SubsEditModel {
    private File zipFile = null;
    private int percentage;
    private boolean TransformationConfigurationHasChanged = false;
    private Map<Subtitle, TransformationConfig> subs = new HashMap<>();

    /**
     * Adds a subs File to transform.
     * @param fileName the name of the subs
     * @param file the file to transform
     * @param size the size of the subs file
     * @throws InvalidSubsFileException
     * @throws IOException
     */
    public void addToSubs(String fileName, File file, long size) throws InvalidSubsFileException, IOException {
        WebAppFileUtilities.validateFile(file, fileName, size);
        subs.put(new Subtitle(fileName), new TransformationConfig(file, fileName, 0, false));
        TransformationConfigurationHasChanged = true;
    }

    /**
     * Removes a subs File to transform.
     * @param subtitle
     */
    public void removeFromSubs(Subtitle subtitle) {
        subs.remove(subtitle);
        TransformationConfigurationHasChanged = true;
    }

    /**
     * Returns the subs files to transform.
     * @return a Map containing the subs to transform
     */
    public Map<Subtitle, TransformationConfig> getSubs() {
        return subs;
    }


    /**
     * Transforms the subs files.
     * @throws IOException
     * @throws TransformationException
     */
    public void transform() throws IOException, TransformationException {

        if (TransformationConfigurationHasChanged) {
            WebAppFileUtilities.checkSubsDirectoryLength();
            deleteZipFile();
            MultiTransformation transformation = new MultiTransformation();
            for (Subtitle file : subs.keySet()) {
                TransformationConfig config = subs.get(file);
                config.setPercent(percentage);
                transformation.addTransformation(config);
            }
            transformation.perform();
            zipFile = WebAppFileUtilities.generateZip(transformation.getTransformations(), WORKING_DIRECTORY_NAME);
        }
        TransformationConfigurationHasChanged = false;
    }

    /**
     * Deletes the current zip file containing transformed subs files.
     */
    private void deleteZipFile() {
        if (zipFile != null) {
            zipFile.delete();
        }
    }

    /**
     * returns the current zip file containing transformed subs files.
     * @return the current zip file containing transformed subs files.
     */
    public File getZipFile() {
        return zipFile;
    }


    /**
     * Set the percentage of subs to remove from the subs files.
     * @param percentage
     */
    public void setPercentage(int percentage) {
        this.percentage = percentage;
        TransformationConfigurationHasChanged = true;
    }
}
