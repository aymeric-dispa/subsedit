package subsedit.webapp;
import subsedit.utils.TransformationException;
import subsedit.util.Subtitle;
import subsedit.utils.InvalidSubsFileException;

import java.io.File;
import java.io.IOException;

/**
 * SubsEdit Presenter - MVP design pattern
 */
public class SubsEditPresenter implements SubsEditView.SubsEditViewListener {
    private SubsEditModel model;
    private SubsEditView view;

    public SubsEditPresenter(SubsEditModel model,
                             SubsEditView view) {

        this.model = model;
        this.view = view;
        view.addListener(this);
    }

    @Override
    public void downloadButtonClick() throws IOException, TransformationException {
        model.transform();
        view.downloadButtonClick(model.getZipFile());
    }

    @Override
    public void changePercentage(int percentage) {
        model.setPercentage(percentage);
    }

    @Override
    public void addSub(String name, File file, long size) throws InvalidSubsFileException, IOException {
        model.addToSubs(name, file, size);
        view.updateGrid(model.getSubs().keySet());
    }

    @Override
    public void removeSub(Subtitle subtitle) {
        model.removeFromSubs(subtitle);
        view.updateGrid(model.getSubs().keySet());
    }
}
