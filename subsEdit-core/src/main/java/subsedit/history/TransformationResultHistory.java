package subsedit.history;

import javafx.beans.property.*;
import subsedit.transformation.TransformationResult;

import java.util.Date;

/**
 * TransformationResult contains the information of a former transformation .
 */
public class TransformationResultHistory extends TransformationResult {
    private StringProperty fileName;
    private ObjectProperty<Date> transformationDate;
    private IntegerProperty percentOfSuppression;

    /**
     * Constructor.
     *
     * @param fileName
     * @param transformationDate
     * @param percentOfSuppression
     */
    public TransformationResultHistory(String fileName, Date transformationDate, int percentOfSuppression, byte[] subStream) {
        super(fileName, transformationDate, percentOfSuppression, subStream);
        this.fileName=new SimpleStringProperty(fileName);
        this.transformationDate =new SimpleObjectProperty<Date>(transformationDate);
        this.percentOfSuppression=new SimpleIntegerProperty(percentOfSuppression);
    }

    public TransformationResultHistory(TransformationResult result) {
      this(result.getFileName(),result.getTransformationDate(),result.getPercentOfSuppression(),result.getSubStream());
    }

    /**
     * @return returns the fileNameProperty
     */
    public StringProperty fileNameProperty() {
        return fileName;
    }

    /**
     * @return the conversion Date Property
     */
    public ObjectProperty<Date> transformationDateProperty() {
        return transformationDate;
    }

    /**
     * @return the percentage of suppression property
     */
    public IntegerProperty percentOfSuppressionProperty() {
        return percentOfSuppression;
    }

    /**
     * @return the name of the transformed file
     */
    public String getFileName() {
        return fileName.get();
    }

    /**
     * @return the date of the transformation
     */
    public Date getTransformationDate() {
        return transformationDate.get();
    }

    /**
     * @return the percentage of suppression
     */
    public int getPercentOfSuppression() {
        return percentOfSuppression.get();
    }


}








