package subsedit.history;

import subsedit.transformation.TransformationResult;

import java.io.Serializable;
import java.util.Date;

/**
 * TransformationResultSerializable permits to serialize the information of a transformation.
 * Created by Aymeric on 13-12-15.
 */

public class TransformationResultSerializable implements Serializable {
    private String fileName;
    private Date transformationDate;
    private int percentOfSuppression;
    private static final long serialVersionUID = 42L;

    /**
     * Constructor.
     * @param properties
     */
    public TransformationResultSerializable(TransformationResult properties){
        this.fileName=properties.getFileName();
        this.transformationDate =properties.getTransformationDate();
        this.percentOfSuppression=properties.getPercentOfSuppression();
    }

    /**
     *
     * @return the name of transformed file.
     */
    public String getFileName() {
        return fileName;
    }

    /**
     *
     * @return the date of the transformation
     */
    public Date getTransformationDate() {
        return transformationDate;
    }

    /**
     *
     * @return the percentage of suppression.
     */
    public int getPercentOfSuppression() {
        return percentOfSuppression;
    }


}
