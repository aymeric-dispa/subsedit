package subsedit.history;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import org.apache.log4j.Logger;
import subsedit.transformation.TransformationResult;

import java.io.*;
import java.util.ArrayList;

/**
 * HistoryHelper is a class which permits to save/load the editions information to/ from a CONFIG_FILE.
 */
public class HistoryHelper {
    final static Logger logger = Logger.getLogger(HistoryHelper.class);
    private static ObservableList<TransformationResultHistory> history = FXCollections.observableArrayList();
    private static final File CONFIG_FILE = new File(System.getProperty("user.home") + "/.subtitleTransformer/subtitleTransformation.history");
    private static final File CONFIG_DIRECTORY_FILE = new File(System.getProperty("user.home") + "/.subtitleTransformer");
    private static boolean isListening = true;

    static {
        checkAndcreateHistoryFiles();
        refreshObservableList();
        history.addListener(new ListChangeListener<TransformationResult>() {
            @Override
            public void onChanged(Change<? extends TransformationResult> c) {
                if (isListening) {
                    ArrayList<TransformationResultSerializable> fileHistory = readHistory();
                    try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(CONFIG_FILE))) {
                        while (c.next()) {
                            if (c.wasAdded()) {
                                for (TransformationResult result : c.getAddedSubList()) {
                                    fileHistory.add(new TransformationResultSerializable(result));
                                }
                            }
                            oos.writeObject(fileHistory);
                        }
                    } catch (IOException ex) {
                        logger.error(ex);

                    }
                    //It happens when another instance of the application modifies subs.
                    if (fileHistory.size() != history.size()) {
                        isListening = false;
                        history.removeAll(history);
                        refreshObservableList();
                        isListening = true;
                    }

                }
            }
        });
    }

    /**
     * Read the history CONFIG_FILE
     *
     * @return the history list
     */
    private static ArrayList<TransformationResultSerializable> readHistory() {
        ArrayList<TransformationResultSerializable> fileHistory = new ArrayList<>();

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(CONFIG_FILE))) {
            fileHistory = (ArrayList<TransformationResultSerializable>) ois.readObject();
        } catch (EOFException e) {
            logger.info("history is empty");
        } catch (IOException | ClassNotFoundException e) {
            logger.error(e);
            e.printStackTrace();
        }
        return fileHistory;
    }

    /**
     * update the history list.
     */
    private static void refreshObservableList() {

        for (TransformationResultSerializable result : readHistory()) {
            history.add(new TransformationResultHistory(result.getFileName(), result.getTransformationDate(), result.getPercentOfSuppression(),null));
        }


    }

    /**
     * Check or create the necessary files of the applications.
     */
    private static void checkAndcreateHistoryFiles() {
        if (!CONFIG_DIRECTORY_FILE.exists() || !CONFIG_FILE.exists()) {
            try {
                CONFIG_DIRECTORY_FILE.mkdir();
                CONFIG_FILE.createNewFile();
            } catch (IOException ex) {
                logger.error(ex);
            }
        }
    }


    /**
     * @return the current history
     */
    public static ObservableList<TransformationResultHistory> getHistory() {
        return history;
    }

    /**
     * save the result
     *
     * @param result
     */
    public static void save(TransformationResultHistory result) {
        history.add(result);
    }


}
