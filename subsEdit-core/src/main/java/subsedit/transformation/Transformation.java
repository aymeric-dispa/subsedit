package subsedit.transformation;

import subsedit.utils.TransformationException;

import java.io.IOException;


public interface Transformation {

    void perform() throws IOException, TransformationException;

}
