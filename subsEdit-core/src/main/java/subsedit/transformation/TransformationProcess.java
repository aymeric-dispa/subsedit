package subsedit.transformation;

import subsedit.utils.TransformationException;
import subsedit.utils.FileUtilities;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Random;

import static subsedit.utils.Constants.OLDSUBS_FOLDER_NAME;



public class TransformationProcess {


    private TransformationConfig config;

    public TransformationProcess(TransformationConfig config) {
        this.config = config;
    }

    /**
     * removes some subtitles from a subtitles file.
     *
     * @return the information of the subtitles multiTransformation.
     * @throws TransformationException
     * @throws IOException
     */
    public TransformationResult transformOneSub() throws TransformationException, IOException {
        if (this.config == null) {
            throw new TransformationException("The configuration file cannot be null ! ");
        }
        int percent = config.getPercent();
        File file = config.getFile();
        ByteArrayInputStream stream=config.getSubtitleStream();
        double total = 0;
        double done = 0;
        String encoding = config.getEncoding();
        boolean isConservable = false;
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(stream, encoding))) {
            String line;
            while ((line = br.readLine()) != null) {
                total++;
                if (line.matches("[0-9]+")) {
                    isConservable = isConservable(percent, total, done);
                }
                if (isConservable) {
                    String myLine = line + System.lineSeparator();
                    os.write(myLine.getBytes(Charset.forName(encoding)));
                    done++;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        stream.reset();
        os.close();

        return new TransformationResult(file.getName(), new Date(), percent, os.toByteArray());

    }

    /**
     * permits to know if a line of text is conservable or not.
     *
     * @param percentageToDelete
     * @param total
     * @param selected
     * @return
     */
    private boolean isConservable(int percentageToDelete, double total, double selected) {
        double deletedPercentage = 100 - ((selected / total) * 100);
        double difference = percentageToDelete - deletedPercentage;
        Random r = new Random();
        return r.nextInt(101) > percentageToDelete + difference;
    }


    /**
     * replace the subtitle file in the system, and save a copy of the former one into another directory.
     */
    protected void stockAndReplaceOldSub(TransformationResult result) throws IOException, TransformationException {
        if (!this.config.isLocalMode()) {
            throw new TransformationException("You can only stock an old subFile in local Mode !");
        }
        FileUtilities.stockAndReplaceFile(this.config.getFile(), OLDSUBS_FOLDER_NAME,result.getSubStream());
    }

}
