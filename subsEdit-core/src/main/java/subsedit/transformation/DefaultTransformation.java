package subsedit.transformation;

import subsedit.utils.TransformationException;

import java.io.IOException;


/**
 * DefaultTransformation is a class which permits to remove a part of the Subtitles file.
 */
public class DefaultTransformation implements Transformation{


    TransformationConfig config;
    TransformationResult result;

    @Override
    public void perform() throws TransformationException, IOException {
        TransformationProcess transformationProcess =new TransformationProcess(config);
        result= transformationProcess.transformOneSub();
        if (config.isLocalMode()) {
            transformationProcess.stockAndReplaceOldSub(result);

        }
    }


    public DefaultTransformation(TransformationConfig config) {
        this.config = config;
    }

    public TransformationResult getResult() {
        return result;
    }

    public TransformationConfig getConfig() {
        return config;
    }




}
