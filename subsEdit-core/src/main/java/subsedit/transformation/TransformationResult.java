package subsedit.transformation;

import java.util.Date;

/**
 * TransformationResult contains the information of a former transformation .
 */
public class TransformationResult {


    private String fileName;
    private Date transformationDate;
    private int percentOfSuppression;
    private byte[] subStream;


    /**
     * Constructor.
     *
     * @param fileName
     * @param transformationDate
     * @param percentOfSuppression
     */
    public TransformationResult(String fileName, Date transformationDate, int percentOfSuppression, byte[] subStream) {
        this.fileName = fileName;
        this.transformationDate = transformationDate;
        this.percentOfSuppression = percentOfSuppression;
        this.subStream = subStream;
    }

    public byte[] getSubStream() {
        return subStream;
    }

    public String getFileName() {
        return this.fileName;

    }
    /**
     * @return the date of the transformation
     */
    public Date getTransformationDate() {
        return this.transformationDate;
    }

    /**
     * @return the percentage of suppression
     */
    public int getPercentOfSuppression() {
        return this.percentOfSuppression;
    }


}




