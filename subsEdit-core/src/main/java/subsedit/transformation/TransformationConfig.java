package subsedit.transformation;

import subsedit.utils.FileUtilities;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

/**
 * MultiTransformation objects contains all the information to use to remove a part from the subs.
 */
public class TransformationConfig {
    private File file;
    private int percent;
    private ByteArrayInputStream subtitleStream;
    private boolean localMode = false;
    private String name;
    private String encoding;

    /**
     * Constructor
     * @param file
     * @param percent
     * @param localMode
     * @throws IOException
     */
    public TransformationConfig(File file, int percent, boolean localMode) throws IOException {
        this.setFile(file);
        this.setPercent(percent);
        this.setLocalMode(localMode);
        this.name=file.getName();
    }

    /**
     * Constructor
     * @param file
     * @param name
     * @param percent
     * @param localMode
     * @throws IOException
     */
    public TransformationConfig(File file, String name, int percent, boolean localMode) throws IOException {
        this(file, percent, localMode);
        this.name = name;
    }

    public void setFile(File file) throws IOException {
        if (!file.isDirectory()) {
            this.file = file;
            this.subtitleStream = new ByteArrayInputStream(org.apache.commons.io.FileUtils.readFileToByteArray(file));
            this.encoding = FileUtilities.detectEncoding(file);
        } else {
            throw new IllegalArgumentException("The file cannot be a Directory");
        }
    }


    public ByteArrayInputStream getSubtitleStream() {
        return subtitleStream;
    }

    public String getEncoding() {
        return encoding;
    }

    public File getFile() {
        return file;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public boolean isLocalMode() {
        return localMode;
    }

    public void setLocalMode(boolean localMode) {
        this.localMode = localMode;
    }

    public String getName() {
        return name;
    }
}
