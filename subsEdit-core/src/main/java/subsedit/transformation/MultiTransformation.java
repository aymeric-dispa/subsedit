package subsedit.transformation;

import org.apache.commons.io.FilenameUtils;
import subsedit.utils.TransformationException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static subsedit.utils.Constants.SRT_EXTENSION;

/**
 * This class permits to remove a part of the subtitles from multiple files.
 */
public class MultiTransformation implements Transformation {

    private ArrayList<DefaultTransformation> defaultTransformations;

    @Override
    public void perform() throws IOException, TransformationException {
        for (DefaultTransformation defaultTransformation : defaultTransformations) {
            defaultTransformation.perform();
        }
    }

    /**
     * Constructor without parameters.
     */
    public MultiTransformation() {
        defaultTransformations = new ArrayList<DefaultTransformation>();
    }

    /**
     * Add a transformationConfig.
     * @param transformationConfig
     */
    public void addTransformation(TransformationConfig transformationConfig) {
        this.defaultTransformations.add(new DefaultTransformation(transformationConfig));
    }

    /**
     * Add multiple transformationConfigs.
     * @param transformationConfigs
     */
    public void addAllTransformations(ArrayList<TransformationConfig> transformationConfigs) {
        for (TransformationConfig config : transformationConfigs){
            this.defaultTransformations.add(new DefaultTransformation(config));
        }
    }

    /**
     * @return the list of transformations to perform.
     */
    public ArrayList<DefaultTransformation> getTransformations() {
        return this.defaultTransformations;
    }

    /**
     * add Directory
     *
     * @return
     */
    public void addDirectory(File file, int percentage, boolean localMode) throws IOException {
        if (file.isDirectory()) {
            File[] filesArray = file.listFiles();
            for (File aFile : filesArray) {
                if (!aFile.isDirectory() && FilenameUtils.getExtension(aFile.getAbsolutePath()).equals(SRT_EXTENSION)) {
                    this.addTransformation(new TransformationConfig(aFile, percentage, localMode));
                }
            }
        }
    }


}
