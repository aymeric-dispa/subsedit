package subsedit.utils;

import org.mozilla.universalchardet.UniversalDetector;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class FileUtilities {

    /**
     * Replace the former subtitle with new one, and keep the former version in another directory.
     * @param file
     * @param destination
     * @param stream
     * @throws IOException
     */
    public static void stockAndReplaceFile(File file, String destination, byte[] stream) throws IOException {
        Path destinationFolder = Paths.get(file.getParent(), destination);
        Path subFile = file.toPath();
        Path destinationFile = Paths.get(file.getParent(), destination, subFile.getFileName().toString());
        if (!Files.exists(destinationFolder)) {
            Files.createDirectory(destinationFolder);
        }
        if (Files.exists(destinationFolder) && Files.isDirectory(destinationFolder) && !Files.exists(destinationFile)) {
            Files.copy(subFile, destinationFile);
        }
        org.apache.commons.io.FileUtils.writeByteArrayToFile(file, stream);

    }


    /**
     * Detect the encoding of the subs file.
     * @param file
     * @return the encoding
     * @throws IOException
     */
    public static String detectEncoding(File file) throws IOException {
        byte[] buf = new byte[4096];
        java.io.FileInputStream fis = new java.io.FileInputStream(file);

        UniversalDetector detector = new UniversalDetector(null);
        int nread;
        while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
            detector.handleData(buf, 0, nread);
        }
        fis.close();
        detector.dataEnd();
        String encoding = detector.getDetectedCharset();
        if (encoding == null) {
            encoding = "UTF-8";
        }
        detector.reset();
        return encoding;
    }





}
