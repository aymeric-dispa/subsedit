package subsedit.utils;

/**
 * Exception used for subs file transformation error
 */
public class TransformationException extends Exception {

    /**
     * Constructor of the TransformationException
     * @param exception the exception
     */
    public TransformationException(String exception) {
        super(exception);
    }
}
