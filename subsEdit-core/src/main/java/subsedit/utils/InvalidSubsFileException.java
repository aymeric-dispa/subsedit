package subsedit.utils;


/**
 * Exception used for subs file validation error
 */
public class InvalidSubsFileException extends Exception {
    public InvalidSubsFileException(String ex){
        super(ex);
    }
}
